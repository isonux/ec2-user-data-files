#!/usr/bin/env bash
sudo apt -y install apache2
sudo systemctl start apache2
sudo su -c "cat > /tmp/index.html <<EOL
<html>
<head><title>My Title</title></head>
<body>
<p>Ony test html file!</p>
</body>
</html>
EOL"
sudo mv /var/www/html/index.html /var/www/html/index.html.orig
sudo mv /tmp/index.html /var/www/html/index.html
sudo systemctl restart apache2
